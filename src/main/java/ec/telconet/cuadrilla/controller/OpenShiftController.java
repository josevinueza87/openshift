package ec.telconet.cuadrilla.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping
public class OpenShiftController {
	Logger log = LogManager.getLogger(this.getClass());

	@GetMapping("listaCuadrilla")
	public String welcome() throws Exception {
		log.info("Petición recibida: listaCuadrilla");		
		return "Welcome";
	}
	
	
	
}
