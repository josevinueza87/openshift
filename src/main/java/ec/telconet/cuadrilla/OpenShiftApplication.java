package ec.telconet.cuadrilla;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.DispatcherServlet;


@SpringBootApplication
@ComponentScan({ "ec.telconet" })
public class OpenShiftApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(OpenShiftApplication.class, args);
	}
	
	
	@Bean
	public ServletRegistrationBean<DispatcherServlet> dispatcherRegistration(DispatcherServlet dispatcherServlet) {
		ServletRegistrationBean<DispatcherServlet> bean = new ServletRegistrationBean<DispatcherServlet>(dispatcherServlet, "/openshift/*");
		bean.setAsyncSupported(true);
		bean.setName("openshift");
		bean.setLoadOnStartup(1);
		return bean;
	}
}
